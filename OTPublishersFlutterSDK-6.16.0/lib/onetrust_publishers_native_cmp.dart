import 'dart:async';

import 'package:flutter/services.dart';

class OTPublishersNativeSDK {
  static const EventChannel _eventChannel = EventChannel("OTPublishersChangeListener");
  static const EventChannel _uiInteractions = EventChannel("OTPublishersUIInteractionListener");
  static const MethodChannel _channel = MethodChannel('onetrust_publishers_native_cmp');

  static Future<bool> startSDK(String storageLocation, String domainIdentifier, String languageCode) async {
    final bool status = await _channel
        .invokeMethod('startSDK', {'storageLocation': storageLocation, 'domainIdentifier': domainIdentifier, 'languageCode': languageCode});
    return status;
  }

  ///Determines whether or not a banner should be shown, based on settings in the OneTrust tenant & whether or not consent was gathered already.
  static Future<bool> shouldShowBanner() async {
    final bool shouldShowBanner = await _channel.invokeMethod('shouldShowBanner');
    return shouldShowBanner;
  }

  ///Returns the consent status for the inputted category; 0 = consent not given, 1 = consent given, -1 = invalid category, or SDK not yet initialized.
  static Future<int> getConsentStatusForCategory(String categoryId) async {
    final int status = await _channel.invokeMethod('getConsentStatusForCategory', {'forCategory': categoryId});
    return status;
  }

  ///Shows the banner atop the current view
  static void showBannerUI() {
    _channel.invokeMethod('showBannerUI');
  }

  ///Shows the preference center atop the current view
  static void showPreferenceCenterUI() {
    _channel.invokeMethod('showPreferenceCenterUI');
  }

  ///Starts listening for consent changes for the category IDs inputted
  static Stream<dynamic> listenForConsentChanges(List<String> categoryIds) {
    return _eventChannel.receiveBroadcastStream({'categoryIds': categoryIds});
  }

  static Stream<dynamic> listenForUIInteractions() {
    return _uiInteractions.receiveBroadcastStream();
  }

  // Deprecated methods //
  ///Initializes OneTrust SDK platform-side and returns download status as a boolean
  @Deprecated('As of 6.16.0, use OTPublishersNativeSDK.startSDK() instead. Init will be removed in a future release.')
  static Future<bool> initOTSDKData(String storageLocation, String domainIdentifier, String languageCode) async {
    final bool status = await _channel
        .invokeMethod('initOTSDKData', {'storageLocation': storageLocation, 'domainIdentifier': domainIdentifier, 'languageCode': languageCode});
    return status;
  }
}
