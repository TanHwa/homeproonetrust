# OTPublishersNativeSDK

Expose OneTrust's Native CMP platform to your Flutter project.

# Getting Started
To add the plugin to your document, add `onetrust_publishers_native_cmp` to your dependencies, indicating the path where you are storing the plugin.

```
dependencies:
  flutter:
    sdk: flutter
  onetrust_publishers_native_cmp:
    path: ../onetrust_publishers_native_cmp/
```

Call `flutter pub get` in your CLI in the project folder.

After adding the plugin to your Flutter project, build the project to check for any immediate errors.

This plugin pulls the OneTrust SDK from mavenCentral and Cocoapods.

### Notes
OTPublishersNativeSDK supports a minimum iOS version of 11.0. You may have to specify a platform target in your `podfile`:
```ruby
platform :ios, '11.0'
```

# Usage
## Initialization
Initialize OneTrust to fetch the data configured in your OneTrust tenant. This will make one or two network calls (depending on the type of template) and deliver a JSON object that contains all of the information required to render a banner and preference center.

The init call is an async function that returns a boolean status value; `true` if the initialization and download of data was successful, `false` if not.

```dart
Future<void> initOneTrust() async{
    bool status;
    try{
      status = await OTPublishersNativeSDK.startSDK("cdn.cookielaw.org","dec6b152-4ad9-487b-8e5a-24a06298417f","en");
    } on PlatformException{
      print("Error communicating with platform-side code");
    }

    if (!mounted) return;

    setState(() {
      _cmpDownloadStatus = status ? 'Success!':'Error';
    });

  }
```
### Arguments
|Name|Type|Description|
|-|-|-|
|storageLocation|String|The CDN location for the JSON that the SDK fetches. (Usually, but not always, `cdn.cookielaw.org`.)
|domainIdentifier|String|The Application guid (retrieved from OneTrust Admin console)|
|languageCode|String|2-digit ISO language code used to return content in a specific langauge.<br></br>**Note:** Any language code format which is not listed in OneTrust environment will be considered as invalid input.<br></br>**Note:** If the languageCode passed by your application is valid, but does not match with a language configuration for your template, then the SDK will return content in the default language configured in OneTrust environment (usually, but not always, English).

## Show an Interface
The plugin can load a Banner or a Preference center using the methods below:

```dart
OTPublishersNativeSDK.showBannerUI(); //load Banner
OTPublishersNativeSDK.showPreferenceCenterUI(); //load Preference Center
```
To determine if a banner should be shown (based on the template rendered and the status of the user's current consent,) use the `shouldShowBanner()` method. This returns a `bool` indicating whether or not a banner should be shown.

```dart
bool _shouldShowBanner;
try{
      _shouldShowBanner = await OTPublishersNativeSDK.shouldShowBanner();
    } on PlatformException{
      print("Error communicating with platform-side code");
    }
    if(_shouldShowBanner){
      OTPublishersNativeSDK.showBannerUI();
    }
```

## Listening for UI Changes
The plugin implements an `EventChannel` that opens a `BroadcastStream` with the platform-side code. To listen for changes in the ui.

Note that:
* The `BroadcastStream` will stay open until it is closed by calling `.cancel()` on it
* Only one `BroadcastStream` per `EventChannel` can be open at a single time. Therefore, you must call `.cancel()` on this stream before calling `.listenForUIInteractions().listen` again.

```dart
var interactionListener = OTPublishersNativeSDK.listenForUIInteractions().listen((event) {
      print(event);
      });
```
Expected return:
```dart
{
    'uiEvent':String,
    'payload':Object
}
```
### UI Events
|Event Name|Description|Payload|
|-|-|-|
|onShowBanner|Triggered when banner is shown|`null`|
|onHideBanner|Triggered when banner is closed|`null`|
|onBannerClickedAcceptAll|Triggered when user allows all consent from banner|`null`|
onBannerClickedRejectAll|Triggered when user rejects all consent from banner|`null`|
onShowPreferenceCenter|Triggered when Preference Center is displayed|`null`|
onHidePreferenceCenter|Triggered when Preference Center is closed|`null`|
onPreferenceCenterAcceptAll|Triggered when user allows all consent from Preference Center|`null`|
onPreferenceCenterRejectAll|Triggered when user rejects all consent from Preference Center|`null`|
onPreferenceCenterConfirmChoices|Triggered when user clicked on save choices after updating consent values from Preference Center|`null`|
onShowVendorList|Triggered when vendor list UI is displayed from an IAB banner/ IAB Preference center|`null`|
onHideVendorList|Triggered when vendor list UI is closed or when back button is clicked|`null`|
onVendorConfirmChoices|Triggered when user updates vendor consent / legitimate interests purpose values and save the choices from vendor list|`null`|
onVendorListVendorConsentChanged|Triggered when user updates consent values for a particular vendor id on vendor list UI|`{vendorId:String, consentStatus:Int}`|
onVendorListVendorLegitimateInterestChanged|Triggered when user updates Legitimate interests values for a particular vendor id on vendor list UI|`{vendorId:String, legitInterest:Int}`|
onPreferenceCenterPurposeConsentChanged|Triggered when user updates consent values for a particular category on Preference Center UI|`{purposeId:String, consentStatus:Int}`|
onPreferenceCenterPurposeLegitimateInterestChanged|Triggered when user updates Legitimate interest values for a particular category on Preference Center UI|`{purposeId:String, legitInterest:Int}`|
allSDKViewsDismissed|Triggered when all the OT SDK Views are dismissed from the view hierarchy.|`{interactionType:String}`|

## When Consent Changes
The consent status is returned as an integer value:
|Status|Description|
|-|-|
|1|Consent given|
|0|Consent not given|
|-1|Consent not gathered or SDK not initialized|
<br></br>
### Query for Consent
To get the present consent state for a category, you can query for consent:

```dart
Future<void> getConsentStatus() async{
    int status;
    try{
      status = await OTPublishersNativeSDK.getConsentStatusForCategory("C0002");
    } on PlatformException{
      print("Error communicating with platform-side code.");
    }
    print("Queried Status for C0002 is = "+status.toString());
  }
```
### Listen for Consent Changes
The plugin implements an `EventChannel` that opens a `BroadcastStream` with the platform-side code. To listen for changes to the consent state.

Note that:
* The `BroadcastStream` will stay open until it is closed by calling `.cancel()` on it
* Only one `BroadcastStream` per `EventChannel` can be open at a single time. Therefore, you must call `.cancel()` on this stream before calling `.listenForConsentChanges().listen` again.

```dart
var consentListener = OTPublishersNativeSDK.listenForConsentChanges(["C0002","C0003"]).listen((event) {
      setCategoryState(event['categoryId'], event['consentStatus']);
      print("New status for "+event['categoryId']+" is "+event['consentStatus'].toString());
    });

    //consentListener.cancel() //cancel listener
```

This listener accepts an array of strings containing Category IDs to listen for. The listener returns an object:
```dart
{
    'categoryId':String
    'consentStatus':int
}
```