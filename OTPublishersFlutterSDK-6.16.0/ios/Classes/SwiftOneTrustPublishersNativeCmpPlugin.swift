import Flutter
import UIKit
import OTPublishersHeadlessSDK

public class SwiftOneTrustPublishersNativeCmpPlugin: NSObject, FlutterPlugin {
  public static func register(with registrar: FlutterPluginRegistrar) {
        let channel = FlutterMethodChannel(name: "onetrust_publishers_native_cmp", binaryMessenger: registrar.messenger())
        let instance = SwiftOneTrustPublishersNativeCmpPlugin()
        registrar.addMethodCallDelegate(instance, channel: channel)
        
        let consentChangeStream = FlutterEventChannel(name: "OTPublishersChangeListener", binaryMessenger: registrar.messenger())
        consentChangeStream.setStreamHandler(OTPublishersChangeListener())
        
        let uiInteractionStream = FlutterEventChannel(name:"OTPublishersUIInteractionListener", binaryMessenger: registrar.messenger())
        uiInteractionStream.setStreamHandler(OTPublishersUIInteractionListener())
    }
    
    public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        if let rootViewController = UIApplication.shared.keyWindow?.rootViewController as? FlutterViewController{
            OTPublishersHeadlessSDK.shared.setupUI(rootViewController, UIType: .none)
            OTPublishersHeadlessSDK.shared.addEventListener(rootViewController)
            
        }
        
        switch call.method{
        case "initOTSDKData":
            guard let arguments = call.arguments else {return}
            if let args = arguments as? [String:String],
               let storageLocation = args["storageLocation"],
               let domainIdentifier = args["domainIdentifier"],
               let languageCode = args["languageCode"]{
                OTPublishersHeadlessSDK.shared.initOTSDKData(storageLocation: storageLocation, domainIdentifier:domainIdentifier, languageCode: languageCode){(status, error) in
                    print("Status = \(status) and error = \(String(describing: error))")
                    result(status)
                }
            }
        case "startSDK":
            guard let arguments = call.arguments else {return}
            if let args = arguments as? [String:String],
               let storageLocation = args["storageLocation"],
               let domainIdentifier = args["domainIdentifier"],
               let languageCode = args["languageCode"]{
                OTPublishersHeadlessSDK.shared.startSDK(storageLocation: storageLocation, domainIdentifier:domainIdentifier, languageCode: languageCode){(otResponse) in
                    print("Status = \(otResponse.status) and error = \(String(describing: otResponse.error))")
                    result(otResponse.status)
                }
            }
        case "shouldShowBanner":
            result(OTPublishersHeadlessSDK.shared.shouldShowBanner())
        case "showBannerUI":
            OTPublishersHeadlessSDK.shared.showBannerUI()
        case "showPreferenceCenterUI":
            OTPublishersHeadlessSDK.shared.showPreferenceCenterUI()
        case "getConsentStatusForCategory":
            guard let arguments = call.arguments else {return}
            if let args = arguments as? [String:String],
               let categoryId = args["forCategory"]{
                result(OTPublishersHeadlessSDK.shared.getConsentStatus(forCategory: categoryId))
            }
        default:
            print("Invalid Method")
        }
    }
    
}

class OTPublishersChangeListener:NSObject, FlutterStreamHandler{
    var emit:FlutterEventSink?
    var error = FlutterError(code: "OneTrustInvalidArgs", message: "Unable to add/remove event listener; invalid parameter passed.", details: "You must listen for specific categories. Eg, pass C0002 to listen for changes to that category")
    
    func onListen(withArguments arguments: Any?, eventSink events: @escaping FlutterEventSink) -> FlutterError? {
        emit = events
        guard let args = arguments as? [String:[String]] else{
            return error
        }
        if let categories = args["categoryIds"]{
            categories.forEach{(catId) in
                NotificationCenter.default.addObserver(self, selector: #selector(listenForChanges(_:)), name: Notification.Name(catId as String), object: nil)
            }
            
        }
        return nil
    }
    
    func onCancel(withArguments arguments: Any?) -> FlutterError? {
        guard let args = arguments as? [String:[String]] else {
            return error
        }
        if let category = args["categoryId"]{
            category.forEach{
                NotificationCenter.default.removeObserver(Notification.Name($0 as String))
            }
            
        }
        return nil
    }
    
    @objc func listenForChanges(_ notification:Notification){
        if let consentStatus = notification.object as? Int{
            emit?(["categoryId":notification.name, "consentStatus": consentStatus])
        }
    }
    
}

class OTPublishersUIInteractionListener:NSObject, FlutterStreamHandler{
    
    static var emit:FlutterEventSink?
    func onListen(withArguments arguments: Any?, eventSink events: @escaping FlutterEventSink) -> FlutterError? {
        FlutterViewController.emit.event = events
        return nil
    }
    
    func onCancel(withArguments arguments: Any?) -> FlutterError? {
        FlutterViewController.emit.event = nil
        return nil
    }
    
}



extension FlutterViewController:OTEventListener{
    public struct emit{
        static var event:FlutterEventSink?
    }
    
    private class eventData{
        let eventName:String
        let payload:[String:Any]?
        
        init(eventName:String, payload:[String:Any]?=nil) {
            self.eventName = eventName
            self.payload = payload
        }
        
        public func format() -> [String:Any]{
            return ["uiEvent":eventName, "payload":payload as Any]
        }
    }
    
    public func onHideBanner() {emit.event?(eventData(eventName: "onHideBanner").format())}
    public func onShowBanner() {emit.event?(eventData(eventName: "onShowBanner").format())}
    public func onBannerClickedRejectAll() {emit.event?(eventData(eventName: "onBannerClickedRejectAll").format())}
    public func onBannerClickedAcceptAll() {emit.event?(eventData(eventName: "onBannerclickedAcceptAll").format())}
    public func onShowPreferenceCenter() {emit.event?(eventData(eventName: "onShowPreferenceCenter").format())}
    public func onHidePreferenceCenter() {emit.event?(eventData(eventName: "onHidePreferenceCenter").format())}
    public func onPreferenceCenterRejectAll() {emit.event?(eventData(eventName: "onPreferenceCenterRejectAll").format())}
    public func onPreferenceCenterAcceptAll() {emit.event?(eventData(eventName: "onPreferenceCenterAcceptAll").format())}
    public func onPreferenceCenterConfirmChoices() {emit.event?(eventData(eventName: "onPreferenceCenterConfirmChoices").format())}
    public func onPreferenceCenterPurposeLegitimateInterestChanged(purposeId: String, legitInterest: Int8) {
        emit.event?(eventData(eventName: "onPreferenceCenterPurposeLegitimateInterestChanged", payload: ["purposeId":purposeId, "legitInterest":legitInterest]).format())
    }
    public func onPreferenceCenterPurposeConsentChanged(purposeId: String, consentStatus: Int8) {
        emit.event?(eventData(eventName: "onPreferenceCenterPurposeConsentChanged", payload: ["purposeId":purposeId, "consentStatus":consentStatus]).format())
    }
    public func onShowVendorList() {emit.event?(eventData(eventName: "onShowVendorList").format())}
    public func onHideVendorList() {emit.event?(eventData(eventName: "onHideVendorList").format())}
    public func onVendorListVendorConsentChanged(vendorId: String, consentStatus: Int8) {
        emit.event?(eventData(eventName: "onVendorListVendorConsentChanged", payload: ["vendorId":vendorId,"consentStatus":consentStatus]).format())
    }
    public func onVendorListVendorLegitimateInterestChanged(vendorId: String, legitInterest: Int8) {
        emit.event?(eventData(eventName: "onVendorListVendorLegitimateInterestChanged", payload: ["vendorId":vendorId,"legitInterest":legitInterest]).format())
    }
    public func onVendorConfirmChoices() {emit.event?(eventData(eventName: "onVendorConfirmChoices").format())}
    public func allSDKViewsDismissed(interactionType: ConsentInteractionType) {
        emit.event?(eventData(eventName: "allSDKViewsDismissed", payload: ["interactionType":interactionType.description as Any]).format() )
    }
    
}
