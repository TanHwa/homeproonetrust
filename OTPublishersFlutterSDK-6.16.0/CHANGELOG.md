## 6.16.0
* Replaced `initOTSDKData` with `startSDK` method
* Fixed force casting issue on Android

## 6.14.0
* Updated OTSDK version
* Added allSDKViewsDismissed event for Android

## 6.13.0
* Update versioning code
* Fixed crash when moving toggles on Android
* Updated to 6.13.0 SDK
* Added example of how to call different IDs for Android and iOS

## 6.12.0
Initial release is of version 6.13.0 to match the OneTrust SDK versioning. Exposes methods of the OTPublishersSDK to:
* Generate a UI
* Prompt users for consent
* Query saved consent status
* Subscribe to consent changes

