import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:io' show Platform;

import 'package:flutter/services.dart';
import 'package:onetrust_publishers_native_cmp/onetrust_publishers_native_cmp.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _cmpDownloadStatus = 'Waiting';
  int _C0003Status = -1;
  int _C0004Status = -1;
  @override
  void initState() {
    super.initState();
    initOneTrust();
    startListening();
  }

  void startListening() {
    var consentListener =
        OTPublishersNativeSDK.listenForConsentChanges(["C0003", "C0004"])
            .listen((event) {
      setCategoryState(event['categoryId'], event['consentStatus']);
      print("New status for " +
          event['categoryId'] +
          " is " +
          event['consentStatus'].toString());
    });

    var interactionListener =
        OTPublishersNativeSDK.listenForUIInteractions().listen((event) {
      print(event);
    });

    //consentListener.cancel(); //Cancel event stream before opening a new one
  }

  Future<void> initOneTrust() async {
    bool status;
    String appId;
    bool shouldShowBanner;

    if (Platform.isAndroid) {
      appId = "ce9f9db7-2b00-4ec3-b866-79a0a6842ef8-test";
    } else if (Platform.isIOS) {
      appId = "162cfe19-aff6-4d60-b10e-6e7b6fdcfb8b-test";
    } else {
      Exception("Platform not found!");
      return;
    }
    try {
      status = await OTPublishersNativeSDK.startSDK("otcc-demo.otprivacy.com",
          "d3605df0-cd30-4966-aef1-10415e1d21ae", "en");
      shouldShowBanner = await OTPublishersNativeSDK.shouldShowBanner();
    } on PlatformException {
      print("Error communicating with platform code");
    }

    if (status && shouldShowBanner) {
      OTPublishersNativeSDK.showBannerUI();
    }

    if (!mounted) return;

    setState(() {
      _cmpDownloadStatus = status ? 'Success!' : 'Error';
    });
  }

  void setCategoryState(String category, int status) {
    setState(() {
      switch (category) {
        case "C0003":
          _C0003Status = status;
          break;
        case "C0004":
          _C0004Status = status;
          break;
        default:
          break;
      }
    });
  }

  Future<void> getConsentForC2() async {
    int status;
    try {
      status = await OTPublishersNativeSDK.getConsentStatusForCategory("C0004");
    } on PlatformException {
      print("Error communicating with platform-side code.");
    }
    print("Queried Status for C0004 is = " + status.toString());
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('OneTrust Plugin Demo App'),
        ),
        body: Center(
          child: Column(
            children: [
              Text('OneTrust Download Status: $_cmpDownloadStatus\n'),
              RaisedButton(
                  onPressed: () {
                    OTPublishersNativeSDK.showBannerUI();
                  },
                  child: Text("Load Banner")),
              RaisedButton(
                  onPressed: () {
                    OTPublishersNativeSDK.showPreferenceCenterUI();
                  },
                  child: Text("Load Preference Center")),
              Text('Category C0003 Status: $_C0003Status\n'),
              Text('Category C0004 Status: $_C0004Status\n')
            ],
          ),
        ),
      ),
    );
  }
}
